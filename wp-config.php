<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'portfolio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l}EY=S2V@>!$W6M][p~fzbVj03#>2(}.C7_$.5vpV/+F|4 Os`v=TuUNG)y;P1~l');
define('SECURE_AUTH_KEY',  'eH0IdOj^IjiD:Af=rhAsd,}kYW+oH.=a.v  ~}anTM#r6hmbVQ $qi0m0Gm:EYlJ');
define('LOGGED_IN_KEY',    '?nUt!0@y:C><U`Pju[F1&SB-tR0o^qCE-gU=egwY%&<]+C yQ6J=X#*MJ-MT#r2q');
define('NONCE_KEY',        'R2jec/t wR~t*/q[qOKR2Q1$%]8wLw ap(DavR(rhGwx7Re;Y_5I8)<Rj.1-uAI4');
define('AUTH_SALT',        'j!T/w>(?8d^hC9&>4#cj-dRH~A7>2h%eq53QT`b#S<A8-;OnGRR$wA4=]FM_P^s6');
define('SECURE_AUTH_SALT', '2X.be6tvm!y^wW w?cz?pJ&1-;~ARt/?0seA;Jchq6ST3h]z(BLi5sQyB8mrCW:#');
define('LOGGED_IN_SALT',   'gTaIA*_l#^8@$}jh0p:;i<5hYGUKz=0xr/ Z|2!V`FRe/!OP-+O @<8?NW&Md0y4');
define('NONCE_SALT',       '_6On,%:ZZd`e<sVZ!/BuVm1NftTr=RpkVixm7>lo$K;XSdD>6V7xEdpy:/O~+8mi');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_1';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
