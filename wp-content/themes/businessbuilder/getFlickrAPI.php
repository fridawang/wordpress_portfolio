<?php
/**
 * /* Template Name: CustomPageT1 
 *
 * 
 *
 * @package businessbuilder
 */

get_header(); 
?>
	<script type="text/javascript" src="../js/jquery-1.11.3.min.js" ></script>
	<script type="text/javascript" src="../js/highcharts.js"></script>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main full-width" role="main">
		   <div id="div-chart1" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
		   <div id="div-chart2" style="min-width: 500px; height: 400px; margin: 0 auto"></div>		
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
		<script type="text/javascript">
			$(function() {
				  // make a jQuery AJAX call to API 1
				  var api1 = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&api_key=4ac14b39a202f841db6890b4b19ce947&tags=baseball';
				  var api2 = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&api_key=4ac14b39a202f841db6890b4b19ce947&tags=football';

				  /*
							Asynchronous ajax call success function
							*/

				  $.ajax({
					url: api1,
					success: function(result) {
					  // build object to render in chart 
					  /*
								result.photos.photo is an Array that contains all of data
								titlecount1 is chartData
								*/
					  data1 = findCount(result.photos.photo);
					  // render the chart with object data and id 
					  render(data1, 1);
					}
				  });

				  $.ajax({
					url: api2,
					success: function(result) {
					  data2 = findCount(result.photos.photo);
					  render(data2, 2)
					}
				  });
				  /*


							*/
				  function findCount(data) {
					//access photo array
					var innerData = [];
					var outerData = [];
					var titleCount = {};
					var titleName = {}
					var colors = Highcharts.getOptions().colors;

					//get the first 30 photos

					for (var i = 0; i < 30; i++) {
					  //if titleCount doesn't have title, build a new one
					  if (!titleCount[data[i].title.length]) {
						titleCount[data[i].title.length] = 1;
						//if have, ++
					  } else {
						titleCount[data[i].title.length]++;
					  }
					}
					for (var i = 0; i < 30; i++) {
					  //if titleCount doesn't have title, build a new one
					  if (!titleName[data[i].title.length]) {
						titleName[data[i].title] = 1;
						//if have, ++
					  } else {
						titleName[data[i].title]++;
					  }
					}
					for (var j = 0; j < Object.keys(titleName).length; j++) {
					  outerData.push({
						name: Object.keys(titleName)[j],
						y: Object.values(titleName)[j],
						color: colors[j]
					  })
					  outerData.sort(function(a, b){
						return a.name.length - b.name.length;
					  });
					}
					/*
								titleCount : {
									{title1: 5},
									{title2: 10},
								}
								object.key(titleCount) = ['title1','title2']
							*/
					for (var k = 0; k < Object.keys(titleCount).length; k++) {
					  innerData.push({
						name: Object.keys(titleCount)[k],
						y: Object.values(titleCount)[k],
						color: colors[k]
					  })
					}
					//chartData is an array of ojbect
					return [innerData, outerData];
				  };

				  function render(data, id) {
					$('#div-chart' + id).highcharts({
					  chart: {
						type: 'pie'
					  },
					  title: {
						text: 'Propeller Interview prepared by Frida Wang'
					  },
					  subtitle: {
						text: 'Source: <a href="https://www.flickr.com/m/">flickr.com</a>'
					  },
					  yAxis: {
						title: {
						  text: 'Pic title length count'
						}
					  },
					  plotOptions: {
						pie: {
						  shadow: false,
						  center: ['50%', '50%']
						}
					  },
					  tooltip: {
						valueSuffix: '%'
					  },
					  series: [{
						name: 'title length count',
						data: data[0],
						size: '60%',
						dataLabels: {
						  formatter: function() {
							return this.y > 5 ? this.name : null;
						  },
						  color: '#ffffff',
						  distance: -30
						}
					  }, {
						name: 'Details of title name',
						data: data[1],
						size: '80%',
						innerSize: '60%',
						dataLabels: {
						  formatter: function() {
							// display only if larger than 1
							return this.y >= 1 ? '<b>' + this.point.name + ':</b> ' +
							  this.y + '%' : null;
						  }
						},
						id: 'Details'
					  }],
					  responsive: {
						rules: [{
						  condition: {
							maxWidth: 400
						  },
						  chartOptions: {
							series: [{
							  id: 'Details',
							  dataLabels: {
								enabled: false
							  }
							}]
						  }
						}]
					  }
					});
				  }

				});
			</script>
		</main><!-- #main -->

	</div><!-- #primary -->

<?php
get_footer();
