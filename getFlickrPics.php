<?php  /* Template Name: CustomPageT1 */
/**
 * Handles Comment Post to WordPress and prevents duplicate comment posting.
 *
 * @package WordPress
 */

$request = wp_remote_get( 'https://api.flickr.com/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&api_key=4ac14b39a202f841db6890b4b19ce947&tags=baseball' );
if( is_wp_error( $request ) ) {
	return false; // Bail early
}
$body = wp_remote_retrieve_body( $request );
$data = json_decode( $body );
if( ! empty( $data ) ) {
	
	echo '<ul>';
	foreach( $data->products as $product ) {
		echo '<li>';
			echo '<a href="' . esc_url( $product->info->link ) . '">' . $product->info->title . '</a>';
		echo '</li>';
	}
	echo '</ul>';
}